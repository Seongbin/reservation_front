const getUserTokenWithHeader = () => {
  const token = sessionStorage.getItem("Authorization");
  if (!token) {
    return false;
  }

  return {
    headers: {
      Authorization: token,
    },
  };
}

export default getUserTokenWithHeader;