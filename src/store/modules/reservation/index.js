import actions from "@/store/modules/reservation/reservation.actions.js";
import getters from "@/store/modules/reservation/reservation.getters.js";
import mutations from "@/store/modules/reservation/reservation.mutations.js";

export default {
  namespaced: true,
  actions,
  getters,
  mutations
}