export default {
  hotels: (status) => status.hotels,
  hotel: (status) => status.hotel,
  selectReservationHotelRooms: (status) => status.selectReservationHotelRooms, // 비어있는 방들 데이터
  selectReservationDates: (status) => status.selectReservationDates,
  myReservationHotelListState: (status) => status.myReservationHotelListState,
}