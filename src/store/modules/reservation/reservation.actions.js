import axios from "axios";
import router from "@/router";
import tokenHeader from "@/util/getUserTokenWithHeader.js";
import * as types from "@/store/modules/reservation/reservation.types";

export default {
  getHotelList({ commit }) {
    commit(types.LOAD_HOTEL_LIST_PENDING)

    axios.get("http://127.0.0.1:8081/hotels").then(obj => {
      console.log(obj.data);
      const payload = {
        data: obj.data
      }
      commit(types.LOAD_HOTEL_LIST_SUCCESS, payload);
    }).catch(err => {
      commit(types.LOAD_HOTEL_LIST_FAIL)
      console.log(err)
    })
  },
  getOneHotel({ commit }, hotelId) {
    console.log("getOneHotel");
    commit(types.LOAD_HOTEL_ONE_PENDING);
    axios.get(`http://127.0.0.1:8081/hotels/${hotelId}`).then(obj => {
      console.log(obj.data);
      const payload = {
        data: obj.data
      }
      commit(types.LOAD_HOTEL_ONE_SUCCESS, payload);
    }).catch(err => {
      console.log(err);
      commit(types.LOAD_HOTEL_ONE_FAIL);
      router.push({ name: "NotFound" })
    });
  },
  reservationSelectDelete({ commit }) {
    commit(types.LOAD_HOTEL_RESERVATION_SELECT_DELETE);
  },
  reservationSelect({ commit }, reservationSelectForm) {
    console.log("actions");
    commit(types.LOAD_HOTEL_RESERVATION_SELECT_PENDING);

    const config = {
      params: {
        reservationStart: reservationSelectForm.reservationStart,
        reservationEnd: reservationSelectForm.reservationEnd,
      }
    }
    axios.get(`http://127.0.0.1:8081/getCurrentReservation/${reservationSelectForm.hotelId}`, config).then(obj => {
      console.log("###", obj.data);
      // 비어있는 방들 데이터
      // TODO 방 없을때 표시 

      const payload = {
        data: obj.data,
        dates: config.params,
      }
      console.log(payload);
      commit(types.LOAD_HOTEL_RESERVATION_SELECT_SUCCESS, payload);
    }).catch(err => {
      console.log(err);
      commit(types.LOAD_HOTEL_RESERVATION_SELECT_FAIL);
    })
  },
  reservationThisRoom({ commit, getters }, key) {
    console.log("#", getters.selectReservationDates);
    const matchRoom = getters.selectReservationHotelRooms.find(obj => obj.hotelRoomId == key);    // console.log("##", matchRoom);
    if (matchRoom == null) {
      return null;
    }

    //todo header 입력
    const token = sessionStorage.getItem("Authorization");
    console.log(token);
    commit(types.HOTEL_RESERVATION_PENDING);

    if (!token) {
      return false;
    }

    const config = {
      headers: {
        Authorization: token,
      },
    }

    const data = {
      // userId: 1,
      hotelInfoHotelId: matchRoom.hotelId,
      hotelRoomHotelRoomId: matchRoom.hotelRoomId,
      startDate: getters.selectReservationDates.reservationStart,
      endDate: getters.selectReservationDates.reservationEnd

    }
    axios.post("http://127.0.0.1:8081/hotels/reservation", data, config).then(obj => {
      console.log(obj)
      commit(types.HOTEL_RESERVATION_SUCCESS)
    }).catch(err => {
      commit(types.HOTEL_RESERVATION_FAIL);
      console.log(err);

    });
  },

  myReservationHotelList({ commit }) {
    console.log("myReservationHotelList");
    const token = tokenHeader();
    console.log(token);
    if (token == false) {
      console.log("not logged in");
      return;
    }
    commit(types.MY_RESERVATION_HOTEL_LIST_PENDING);
    axios.get("http://127.0.0.1:8081/myReservation", token).then(obj => {
      console.log("result");
      console.log(obj.data);
      const payload = {
        data: obj.data,
      }
      commit(types.MY_RESERVATION_HOTEL_LIST_SUCCESS, payload);

    }).catch(err => {
      console.log(err);
      commit(types.MY_RESERVATION_HOTEL_LIST_FAIL)
    })
  },

  myReservationHotel({ commit, dispatch }, reservationId) {
    const token = tokenHeader();
    console.log(token);
    if (token == false) {
      console.log("not logged in");
      return;
    }
    console.log("test")
    commit(types.MY_RESERVATION_HOTEL_LIST_PENDING);
    axios.get(`http://127.0.0.1:8081/myReservation/${reservationId}`, token).then(obj => {
      console.log("result");
      console.log(obj);
      const payload = {
        data: obj.data,
      }
      commit(types.MY_RESERVATION_HOTEL_SUCCESS, payload);

    }).catch(err => {
      const errorData = err.response.data;
      const payload = {
        data: errorData,
      }
      dispatch("errorState/businessError", payload, { root: true });
      router.push({ name: "PageHome" })
    })
  },

  deleteMyReservation({ commit, dispatch }, reservationId) {
    commit(types.MY_RESERVATION_HOTEL_CANCEL_PENDING);
    const token = tokenHeader();
    console.log(token);
    if (token == false) {
      console.log("not logged in");
      return;
    }
    axios.post(`http://127.0.0.1:8081/deleteMyReservation`, { reservationId }, token).then(obj => {
      console.log(obj)
      commit(types.MY_RESERVATION_HOTEL_CANCEL_SUCCESS);
      router.push({ name: "PageHome" });
    }
    ).catch(err => {
      console.log(err.response.data);
      const errorData = err.response.data;
      const payload = {
        data: errorData,
      }
      commit(types.MY_RESERVATION_HOTEL_CANCEL_FAIL);
      dispatch(`errorState/businessError`, payload, { root: true });
    });
  },
  changeMyReservation({ commit, dispatch, getters }, reservationId) {
    const token = tokenHeader();
    console.log(token);
    if (token == false) {
      console.log("not logged in");
      return;
    }
    const matchRoom = getters.selectReservationHotelRooms[0];//.find(obj => obj.hotelRoomId == reservationId);    // console.log("##", matchRoom);
    console.log("##" , matchRoom);
    if (matchRoom == null) {
      return null;
    }

    const data = {
      // userId: 1,
      reservationId ,
      hotelInfoHotelId: matchRoom.hotelId,
      hotelRoomHotelRoomId: matchRoom.hotelRoomId,
      startDate: getters.selectReservationDates.reservationStart,
      endDate: getters.selectReservationDates.reservationEnd

    }
    commit(types.MY_RESERVATION_HOTEL_CHANGE_PENDING);
    console.log(data);
    axios.post("http://127.0.0.1:8081/changeMyReservation", data, token).then(obj => {
      commit(types.MY_RESERVATION_HOTEL_CHANGE_SUCCESS);
      console.log(obj);
    }).catch(err => {

      commit(types.MY_RESERVATION_HOTEL_CHANGE_FAIL);
      console.log(err);
    })

    console.log(dispatch);
    console.log(reservationId);

  }
}