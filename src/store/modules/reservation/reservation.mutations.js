import * as types from "@/store/modules/reservation/reservation.types";

export default {
  [types.LOAD_HOTEL_LIST_SUCCESS](status, payload) {
    status.hotels = payload.data;
  },
  [types.LOAD_HOTEL_ONE_SUCCESS](status, payload) {
    status.hotel = payload.data;
  },
  [types.LOAD_HOTEL_RESERVATION_SELECT_DELETE](status) {
    status.selectReservationHotelRooms = null;
    status.selectReservationDates = null;
  },
  [types.LOAD_HOTEL_RESERVATION_SELECT_SUCCESS](status, payload) {
    status.selectReservationHotelRooms = payload.data;
    status.selectReservationDates = payload.dates;
  },
  [types.MY_RESERVATION_HOTEL_LIST_SUCCESS](status, payload) {
    status.myReservationHotelListState = payload.data;
  },
  [types.MY_RESERVATION_HOTEL_SUCCESS](status, payload) {
    status.myReservationHotelState = payload.data;
    console.log(status);
  },
}