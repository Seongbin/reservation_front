import actions from "@/store/modules/error/error.actions.js";
import getters from "@/store/modules/error/error.getters.js";
import mutations from "@/store/modules/error/error.mutations.js";

export default {
  namespaced: true,
  actions,
  getters,
  mutations,
}