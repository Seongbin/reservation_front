import * as types from "@/store/modules/error/error.types";

export default {
  [types.BUSINESS_ERROR](status, payload) {
    status.businessError = payload.data;
  },
  [types.BUSINESS_ERROR_DELETE](status) {
    status.businessError = null;
  }
}