import * as types from "@/store/modules/error/error.types";

export default {
  async businessError({ commit }, error) {
    const payload = {
      data: error,
    }
    commit(types.BUSINESS_ERROR, payload)
  },
  businessErrorDelete({ commit }) {
    commit(types.BUSINESS_ERROR_DELETE)
  }
}