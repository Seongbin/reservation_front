import * as types from "@/store/modules/auth/auth.types";

export default {
  [types.LOGIN_USER_PENDING]() {
    console.log("LOGIN_USER_PENDING");
  },
  [types.LOAD_USER_PENDING]() {
    console.log("LOAD_USER_PENDING");
  },
  [types.LOGIN_USER_SUCCESS](status, payload) {
    status.jwt = payload.jwt;
    sessionStorage.setItem("Authorization", payload.jwt);
  },
  [types.LOAD_USER_SUCCESS](status, payload) {
    status.userData = payload;
  },
  [types.CLEAR_AUTH](status) {
    status.userData = null;
    window.sessionStorage.removeItem("Authorization");
  }
}