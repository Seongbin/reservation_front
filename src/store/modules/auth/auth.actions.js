import axiosConfig from "@/config/axios.config";
import router from "@/router";
import * as types from "@/store/modules/auth/auth.types";

export default {
  async login({ commit, dispatch }, formData) {
    commit(types.LOGIN_USER_PENDING);
    try {
      const result = await axiosConfig.post("/signIn", formData);
      console.log(result.data);
      const payload = {
        jwt: result.data
      }
      commit(types.LOGIN_USER_SUCCESS, payload);
      dispatch("loadUser");
    } catch (err) {
      console.log(err);
      commit(types.LOGIN_USER_FAIL);
      dispatch("logout");
    }
  },

  async loadUser({ commit, dispatch }) {
    try {

      const token = sessionStorage.getItem("Authorization");
      console.log(token);
      commit(types.LOAD_USER_PENDING);

      if (!token) {
        commit(types.LOAD_USER_FAIL);
        return false;
      }

      const result = await axiosConfig.post("loadUser");
      console.log(result.data);
      commit(types.LOAD_USER_SUCCESS, result.data);
      router.push({ name: 'PageHome' }).catch((err) => {
        throw new Error(`Problem handling something: ${err}.`);
      });
    } catch (err) {
      console.log(err);
      commit(types.LOAD_USER_FAIL);
      dispatch("logout");
    }
  },

  async signUp({ commit, dispatch }, formData) {
    commit(types.REGISTER_PENDING);
    try {
      const result = await axiosConfig.post("/signUp", formData);
      commit(types.REGISTER_SUCCESS);
      dispatch("loadUser", result.data);
    } catch (err) {
      console.log(err);
      commit(types.LOGIN_USER_FAIL)
      dispatch("logout");
    }
  },

  async logout({ commit }) {
    console.log("commit logout")
    commit(types.CLEAR_AUTH);
    router.push({ name: 'PageHome' }).catch((err) => {
      throw new Error(`Problem handling something: ${err}.`);
    });
  }
}