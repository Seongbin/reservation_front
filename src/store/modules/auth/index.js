import actions from "@/store/modules/auth/auth.actions.js";
import getters from "@/store/modules/auth/auth.getters.js";
import mutations from "@/store/modules/auth/auth.mutations.js";

const state = () => {
  return {
    userData: null,
  }
}

export default {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
}