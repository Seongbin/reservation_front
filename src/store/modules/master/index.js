import actions from "@/store/modules/master/master.actions";
import getters from "@/store/modules/master/master.getters";
import mutations from "@/store/modules/master/master.mutations";

const state = () => {
  return {

  }
}

export default {
  namespaced: true,
  actions,
  getters,
  mutations,
  state,
}