import Vue from 'vue'
import Vuex from 'vuex'

import authState from "@/store/modules/auth";
import reservationState from "@/store/modules/reservation";
import errorState from "@/store/modules/error";
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    errorState,
    authState,
    reservationState,
  },
  plugins: [createPersistedState()]
})
