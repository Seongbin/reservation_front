import Vue from 'vue'
import VueRouter from 'vue-router'

//Page
import PageHome from "@/page/front/front";
import Login from "@/page/front/login";
import SignUp from "@/page/service/signUp/signUp";
import NotFound from "@/page/common/notFound";
import Hotel from "@/page/service/reservation/hotel";
import ReservationHotel from "@/page/service/reservation/reservationHotel";

import MyReservation from "@/page/service/customer/myReservation";

//store
import store from "@/store";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'PageHome',
    component: PageHome,
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
    meta: {
      keepAlive: true,
      guestPage: true,
    }
  },
  {
    path: "/signUp",
    name: "SignUp",
    component: SignUp,
    meta: {
      keepAlive: true,
      guestPage: true,
    }
  },
  {
    path: "/hotel/:hotelId",
    name: "Hotel",
    component: Hotel,
  },
  {
    path: "/hotel/reservationHotel/:hotelId",
    name: "reservationHotel",
    component: ReservationHotel,
    meta: {
      requireAuth: true,
    }
  },
  {
    path: "/hotel/myReservation/:reservationId",
    name: "myReservation",
    component: MyReservation,
    meta: {
      requireAuth: true,
    }
  },
  {
    path: "/notFound",
    name: "NotFound",
    component: NotFound,
  },
  {
    path: "*",
    component: NotFound,
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {

  const isLoggedIn = store.getters["authState/isLoggedIn"];
  // const isLoggedIn = sessionStorage.getItem("jwt");
  console.log("router check");
  console.log(isLoggedIn)

  //update관련 해서 가드 넣어줄것
  if (to.matched.some(record => record.meta.requireAuth)) {
    console.log("requireAuth");
    if (isLoggedIn) {
      next();
    } else {
      next({ name: "PageHome" });
      store.state.error=" please login ";
      //next 에 네임 지정 할 수 있다
    }
  } else if (to.matched.some(record => record.meta.guestPage)) {
    console.log("guestPage");

    if (isLoggedIn) {
      next({ name: "PageHome" });
      //next 에 네임 지정 할 수 있다
    } else {
      next();
    }
  } else {
    next();
  }
})
export default router
