import axios from "axios";

const api = axios.create({
  baseURL: "http://localhost:8080"
});

api.interceptors.request.use((config) => {
  const token = sessionStorage.getItem("Authorization");
  // config.headers["Accept-Language"] = sessionStorage["language"] 
  config.headers["Authorization"] = `Bearer ${token}`;
  console.log("config")
  console.log(config);
  return config;
});

export default api;
